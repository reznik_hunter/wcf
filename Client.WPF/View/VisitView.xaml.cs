﻿using Client.WPF.Model.TableLayer;
using Data.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.WPF.View
{
    /// <summary>
    /// Логика взаимодействия для VisitView.xaml
    /// </summary>
    public partial class VisitView : Window
    {

        public VisitView()
        {
            InitializeComponent();
            BaseModelTable<Expert> exp = new BaseModelTable<Expert>();
            BaseModelTable<Patient> pat = new BaseModelTable<Patient>();
            BaseModelTable<Treatment> tre = new BaseModelTable<Treatment>();
            foreach (var obj in exp.CollectionEntity)
            {
                ((ArrayList)window.Resources["experts"]).Add(obj);
            }
            ArrayList ar = ((ArrayList)window.Resources["experts"]);

            foreach (var obj in pat.CollectionEntity)
            {
                ((ArrayList)window.Resources["patients"]).Add(obj);
            }

            foreach (var obj in tre.CollectionEntity)
            {
                ((ArrayList)window.Resources["treatments"]).Add(obj);
            }        
        }
    }
}
