﻿using Client.WPF.Model.EntityLayer;
using Client.WPF.Model.TableLayer;
using Client.WPF.View;
using Client.WPF.ViewModel;
using Client.WPF.ViewModel.EntityLayer;
using Client.WPF.ViewModel.TableLayer;
using Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainContext();
        }
    }
}
