﻿using Client.WPF.ViewModel.TableLayer;
using Data;
using Data.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Client.WPF.Commands.TableLayer
{
    internal class DeleteCommand<T> : ICommand
        where T : EntityBase
    {
        private const int ARE_EQUAL = 0;
        private const int NONE_SELECTED = -1;
        private IViewModelTable<T> _vm;

        public DeleteCommand(IViewModelTable<T> viewModel)
        {
            _vm = viewModel;
            _vm.PropertyChanged += vm_PropertyChanged;
        }

        private void vm_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (string.Compare(e.PropertyName,
                  _vm.GetPropertyName())
                    == ARE_EQUAL)
            {
                CanExecuteChanged(this, new EventArgs());
            }
        }

        public bool CanExecute(object parameter)
        {
            if (_vm.SelectedEntity == null)
                return false;
            return true;
        }

        public event EventHandler CanExecuteChanged
            = delegate { };

        public void Execute(object parameter)
        {
            _vm.Delete();
        }
    }
}
