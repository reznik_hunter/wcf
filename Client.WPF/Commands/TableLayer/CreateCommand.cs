﻿using Client.WPF.ViewModel.TableLayer;
using Data;
using Data.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Client.WPF.Commands.TableLayer
{
    internal class CreateCommand<T> : ICommand
        where T : EntityBase
    {
        private const int ARE_EQUAL = 0;
        private const int NONE_SELECTED = -1;
        private IViewModelTable<T> _vm;

        public CreateCommand(IViewModelTable<T> viewModel)
        {
            _vm = viewModel;
            _vm.PropertyChanged += vm_PropertyChanged;
        }

        private void vm_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            CanExecuteChanged(this, new EventArgs());
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
            = delegate { };

        public void Execute(object parameter)
        {
            _vm.Create();
        }
    }
}
