﻿using Client.WPF.ViewModel.EntityLayer;
using Client.WPF.ViewModel.EntityLayer.EntityData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Client.WPF.Commands.EntityLayer
{
    internal class CreateOrUpdateCommand<T> : ICommand
        where T : IViewEntityData
    {
        private const int ARE_EQUAL = 0;
        private const int NONE_SELECTED = -1;
        private IViewModelEntity<T> _vm;
        private bool _isCreate;
        public CreateOrUpdateCommand(IViewModelEntity<T> viewModel, bool isCreate)
        {
            _isCreate = isCreate;
            _vm = viewModel;
            _vm.PropertyChanged += vm_PropertyChanged;
        }

        private void vm_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (string.Compare(e.PropertyName,
                               _vm.GetPropertyName())
                == ARE_EQUAL)
            {
                CanExecuteChanged(this, new EventArgs());
            }
        }

        public bool CanExecute(object parameter)
        {
            if (_vm.ViewModelData == null)
                return false;
            return (_vm.ViewModelData).Id
                   > NONE_SELECTED;
        }

        public event EventHandler CanExecuteChanged
            = delegate { };

        public void Execute(object parameter)
        {
            if(_isCreate)
            {
                _vm.Create();
            }
            else
            {
                _vm.Update();
            }
        }
    }
}
