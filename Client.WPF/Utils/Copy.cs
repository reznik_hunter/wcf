﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.Utils
{
    public static class Copy
    {
        public static void CopyFieldsFromEntity<T>(this T obj, T entity) 
            where T : IEntityBase
        {
            Type objType = obj.GetType();
            if (objType.ToString() != entity.GetType().ToString())
                return;
            PropertyInfo[] properties = objType.GetProperties();
            foreach (PropertyInfo objProperty in properties)
            {
                PropertyInfo entityProperty = entity.GetType().GetProperty(objProperty.Name);
                objProperty.SetValue(obj, entityProperty.GetValue(entity, null));
            }
        }

        public static void CopyFieldsFrom<T, V>(this T obj, V entity) 
            where T : IEntityBase
            where V : IEntityBase
        {
            Type objType = obj.GetType();
            PropertyInfo[] properties = objType.GetProperties();
            foreach (PropertyInfo objProperty in properties)
            {
                PropertyInfo entityProperty = entity.GetType().GetProperty(objProperty.Name);
                objProperty.SetValue(obj, entityProperty.GetValue(entity, null));
            }
        }
    }
}
