﻿using Client.WPF.Model.EntityLayer;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.Utils
{
    class ModelComparer<T> : IEqualityComparer<IModelEntity<T>> where T : IEntityBase
    {
        public bool Equals(IModelEntity<T> model1, IModelEntity<T> model2)
        {
            if (model1.Entity.Id == model2.Entity.Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetHashCode(IModelEntity<T> model)
        {
            int hCode = model.Entity.Id;
            return hCode.GetHashCode();
        }
    }
}
