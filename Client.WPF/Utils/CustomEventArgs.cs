﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.Utils
{
    public class CustomEventArgs<T> : EventArgs
        where T : IEntityBase
    {
        public T Entity { get; set; }
        public CustomEventArgs(T entity)
        {
            Entity = entity;
        }
    }
}
