﻿using Client.WPF.View;
using Client.WPF.ViewModel.EntityLayer.EntityData;
using Client.WPF.ViewModel.TableLayer;
using Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.ViewModel
{
    class MainContext
    {
        public ViewModelTableFor<Expert, ExpertView, ExpertViewEntityData> ExpertsViewModel { get; set; }
        public ViewModelTableFor<Patient, PatientView, PatientViewEntityData> PatientsViewModel { get; set; }
        public ViewModelTableFor<Treatment, TreatmentView, TreatmentViewEntityData> TreatmentsViewModel { get; set; }
        public ViewModelTableFor<Visit, VisitView, VisitViewEntityData> VisitsViewModel { get; set; }

        public MainContext()
        {
            ExpertsViewModel = new ViewModelTableFor<Expert, ExpertView, ExpertViewEntityData>();
            PatientsViewModel = new ViewModelTableFor<Patient, PatientView, PatientViewEntityData>();
            TreatmentsViewModel = new ViewModelTableFor<Treatment, TreatmentView, TreatmentViewEntityData>();
            VisitsViewModel = new ViewModelTableFor<Visit, VisitView, VisitViewEntityData>();
        }
    }
}
