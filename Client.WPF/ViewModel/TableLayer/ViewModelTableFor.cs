﻿using Client.WPF.Commands.TableLayer;
using Client.WPF.Model.EntityLayer;
using Client.WPF.Model.TableLayer;
using Client.WPF.Utils;
using Client.WPF.View;
using Client.WPF.ViewModel.EntityLayer;
using Client.WPF.ViewModel.EntityLayer.EntityData;
using Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Client.WPF.ViewModel.TableLayer
{
    class ViewModelTableFor<T, V , D> : Notifier, IViewModelTable<T>
        where T : EntityBase
        where V : Window
        where D : IViewEntityData
    {
        private string _propertyName = "Selected" + typeof(T).Name;

        private readonly ICommand _getCommand;
        private readonly ICommand _deleteCommand;
        private readonly ICommand _createCommand;

        private V _view;
        private readonly IModelTable<T> _model;

        private T _selectedEntity;

        public IModelTable<T> Model
        {
            get
            {
                return _model;
            }
        }

        public Dictionary<IModelEntity<T>, int>
             UsedEntityModels { set; get; }

        public ObservableCollection<T>
             CollectionEntity { get { return _model.CollectionEntity; } }

        public object SelectedItem
        {
            set
            {
                if (value == null)
                    return;
                SelectedEntity = value as T;
            }
        }

        public T SelectedEntity
        {
            set
            {
                _selectedEntity = value;
                NotifyPropertyChanged(_propertyName);
            }
            get
            {
                return _selectedEntity;
            }
        }

        public ICommand GetCommand
        {
            get { return _getCommand; }
        }

        public ICommand DeleteCommand
        {
            get { return _deleteCommand; }
        }

        public ICommand CreateCommand
        {
            get { return _createCommand; }
        }

        public ViewModelTableFor()
        {
            UsedEntityModels = new Dictionary<IModelEntity<T>, int>(new ModelComparer<T>());

            _model = new BaseModelTable<T>();
            _getCommand = new GetCommand<T>(this);
            _createCommand = new CreateCommand<T>(this);
            _deleteCommand = new DeleteCommand<T>(this);
        }

        public void Get()
        {
            IModelEntity<T> _modelEntity = new BaseModelEntity<T>(SelectedEntity.Id);

            if (UsedEntityModels.ContainsKey(_modelEntity))
            {
                UsedEntityModels[_modelEntity] += 1;
            }
            else
            {
                UsedEntityModels.Add(_modelEntity, 1);
            }

            _modelEntity = UsedEntityModels.Keys.Where(x => x.Entity.Id == SelectedEntity.Id).SingleOrDefault();

            Type entityType = typeof(V);
            ConstructorInfo ci = entityType.GetConstructor(new Type[] { });
            _view = (V)ci.Invoke(new object[] { });

            _view.DataContext
                = new ViewModelEntityFor<T, D>(this, _modelEntity);
            _view.Show();
        }

        public void Delete()
        {
            IModelEntity<T> _modelEntity = new BaseModelEntity<T>(SelectedEntity.Id);
            _modelEntity.Delete(SelectedEntity.Id);
            _model.Update();
        }

        public void Create()
        {
            Type entityType = typeof(V);
            ConstructorInfo ci = entityType.GetConstructor(new Type[] { });
            _view = (V)ci.Invoke(new object[] { });

            _view.DataContext
                = new ViewModelEntityFor<T, D>(this);
            _view.Show();
        }

        public string GetPropertyName()
        {
            return _propertyName;
        }
    }
}
