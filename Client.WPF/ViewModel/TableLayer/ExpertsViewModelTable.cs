﻿//using Client.WPF.Commands.TableLayer;
//using Client.WPF.Model.EntityLayer;
//using Client.WPF.Model.TableLayer;
//using Client.WPF.Utils;
//using Client.WPF.View;
//using Client.WPF.ViewModel.EntityLayer;
//using Data.Domain;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows.Input;

//namespace Client.WPF.ViewModel.TableLayer
//{
//    class ExpertsViewModelTable : Notifier, IViewModelTable<Experts>
//    {
//        public const string PROPERTY_NAME = "SelectedExpert";

//        private readonly ICommand _getCommand;
//        private readonly ICommand _deleteCommand;
//        private readonly ICommand _createCommand;

//        private ExpertView _view;
//        private readonly IModelTable<Experts> _model;

//        private Experts _selectedEntity;

//        public IModelTable<Experts> Model
//        {
//            get
//            {
//                return _model;
//            }
//        }

//        public Dictionary<IModelEntity<Experts>, int>
//             UsedEntityModels { set; get; }

//        public ObservableCollection<Experts>
//             CollectionEntity { get { return _model.CollectionEntity; } }

//        public object SelectedItem
//        {
//            set
//            {
//                if (value == null)
//                    return;
//                SelectedEntity = value as Experts;
//            }
//        }

//        public Experts SelectedEntity
//        {
//            set
//            {
//                _selectedEntity = value;
//                NotifyPropertyChanged(PROPERTY_NAME);
//            }
//            get
//            {
//                return _selectedEntity;
//            }
//        }

//        public ICommand GetCommand
//        {
//            get { return _getCommand; }
//        }

//        public ICommand DeleteCommand
//        {
//            get { return _deleteCommand; }
//        }

//        public ICommand CreateCommand
//        {
//            get { return _createCommand; }
//        }
        
//        public ExpertsViewModelTable()
//        {
//            UsedEntityModels = new Dictionary<IModelEntity<Experts>, int>(new ModelComparer<Experts>());

//            _model = new BaseModelTable<Experts>();
//            _getCommand = new GetCommand<Experts>(this);
//            _createCommand = new CreateCommand<Experts>(this);
//            _deleteCommand = new DeleteCommand<Experts>(this);
//        }

//        public void Get()
//        {
//            IModelEntity<Experts> _modelEntity = new BaseModelEntity<Experts>(SelectedEntity.Id);

//            if (UsedEntityModels.ContainsKey(_modelEntity))
//            {
//                UsedEntityModels[_modelEntity] += 1;
//            }
//            else
//            {
//                UsedEntityModels.Add(_modelEntity, 1);
//            }

//            _modelEntity = UsedEntityModels.Keys.Where(x => x.Entity.Id == SelectedEntity.Id).SingleOrDefault();

//            _view = new ExpertView();
//            _view.DataContext
//                = new ExpertViewModelEntity(this,_modelEntity);
//            _view.Show();
//        }

//        public void Delete()
//        {
//            IModelEntity<Experts> _modelEntity = new BaseModelEntity<Experts>(SelectedEntity.Id);
//            _modelEntity.Delete(SelectedEntity.Id);
//            _model.Update();
//        }

//        public void Create()
//        {
//            _view = new ExpertView();
//            _view.DataContext
//                = new ExpertViewModelEntity(this);
//            _view.Show();
//        }
//    }
//}
