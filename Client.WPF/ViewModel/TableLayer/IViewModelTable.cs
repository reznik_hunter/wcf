﻿using Client.WPF.Model.TableLayer;
using Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.ViewModel.TableLayer
{
    public interface IViewModelTable<T> : INotifyPropertyChanged 
        where T : IEntityBase
    {
        T SelectedEntity { get; set; }
        IModelTable<T> Model { get; }

        void Get();
        void Create();
        void Delete();

        string GetPropertyName();
    }
}
