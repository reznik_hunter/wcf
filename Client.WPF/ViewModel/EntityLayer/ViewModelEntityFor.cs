﻿using Client.WPF.Commands;
using Client.WPF.Commands.EntityLayer;
using Client.WPF.Model.EntityLayer;
using Client.WPF.Utils;
using Client.WPF.ViewModel.EntityLayer.EntityData;
using Client.WPF.ViewModel.TableLayer;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Client.WPF.ViewModel.EntityLayer
{
    class ViewModelEntityFor<T, D> : Notifier, IViewModelEntity<IViewEntityData>
        where T : EntityBase
        where D : IViewEntityData
    {
        public string PROPERTY_NAME = typeof(T) + "ViewData";

        private readonly IViewModelTable<T> _viewModelTable;
        private readonly IModelEntity<T> _model;

        private IViewEntityData _viewModelData;

        private readonly ICommand _createOrUpdateCommand;

        private bool _details;

        public T Entity
        {
            get
            {
                return _model.Entity;
            }
        }

        public IViewModelTable<T> ViewModelTable
        {
            get { return _viewModelTable; }
        }

        public IViewEntityData ViewModelData
        {
            get { return _viewModelData; }
            set
            {
                _viewModelData = value;
                NotifyPropertyChanged(PROPERTY_NAME);
                Details = true;
            }
        }

        public ICommand CreateOrUpdateCommand
        {
            get { return _createOrUpdateCommand; }
        }

        public bool Details
        {
            get { return _details; }
            set
            {
                _details = value;
                NotifyPropertyChanged("Details");
            }
        }

        public ViewModelEntityFor(IViewModelTable<T> viewModelTable)
        {
            _viewModelTable = viewModelTable;
            _model = new BaseModelEntity<T>();
            _model.Created +=
                model_Сreated;
            _createOrUpdateCommand = new CreateOrUpdateCommand<IViewEntityData>(this, true);

            Type entityType = typeof(D);
            ConstructorInfo ci = entityType.GetConstructor(new Type[] { });
            ViewModelData = (D)ci.Invoke(new object[] { });
        }

        public ViewModelEntityFor(IViewModelTable<T> viewModelTable, IModelEntity<T> model)
        {
            _viewModelTable = viewModelTable;
            _model = model;
            _model.Updated +=
                model_Updated;
            _createOrUpdateCommand = new CreateOrUpdateCommand<IViewEntityData>(this, false);

            Type entityType = typeof(D);
            ConstructorInfo ci = entityType.GetConstructor(new Type[] { _model.Entity.GetType() });
            ViewModelData = (D)ci.Invoke(new object[] { _model.Entity });
        }

        public void Update()
        {
            Type entityType = typeof(T);
            ConstructorInfo ci = entityType.GetConstructor(new Type[] { });
            T entity = (T)ci.Invoke(new object[] { });

            entity.CopyFieldsFrom(ViewModelData);
            _model.Update(ViewModelData.Id, entity);
            _viewModelTable.Model.Update();
        }

        private void model_Updated(object sender,
                                          CustomEventArgs<T> e)
        {
            Entity.CopyFieldsFromEntity(e.Entity);

            if (ViewModelData != null
                && e.Entity.Id == ViewModelData.Id)
            {
                ViewModelData.Update(e.Entity);
            }
        }

        public void Create()
        {
            Type entityType = typeof(T);
            ConstructorInfo ci = entityType.GetConstructor(new Type[] { });
            T entity = (T)ci.Invoke(new object[] { });

            entity.CopyFieldsFrom(ViewModelData);
            _model.Post(entity);
            _viewModelTable.Model.Update();
        }

        private void model_Сreated(object sender,
                                          CustomEventArgs<T> e)
        {
            Entity.CopyFieldsFromEntity(e.Entity);
            Details = false;
        }

        public string GetPropertyName()
        {
            return PROPERTY_NAME;
        }
    }
}
