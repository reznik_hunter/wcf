﻿using Client.WPF.ViewModel.EntityLayer.EntityData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.ViewModel.EntityLayer
{
    public interface IViewModelEntity<T> : INotifyPropertyChanged
        where T : IViewEntityData
    {
        T ViewModelData { get; set; }
        void Update();
        void Create();

        string GetPropertyName();
    }
}
