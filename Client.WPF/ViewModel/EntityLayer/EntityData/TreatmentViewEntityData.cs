﻿using Client.WPF.Utils;
using Data;
using Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.ViewModel.EntityLayer.EntityData
{
    class TreatmentViewEntityData : Notifier, ITreatment, IViewEntityData
    {
        private int _id;
        private string _diagnosis;
        private string _medicine;
        private string _material;

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
            }
        }

        public string Diagnosis {
            get { return _diagnosis; }
            set
            {
                _diagnosis = value;
                NotifyPropertyChanged("Diagnosis");
            }
        }

        public string Medicine {
            get { return _medicine; }
            set
            {
                _medicine = value;
                NotifyPropertyChanged("Medicine");
            }
        }

        public string Material {
            get { return _material; }
            set
            {
                _material = value;
                NotifyPropertyChanged("Material");
            }
        }

        public TreatmentViewEntityData()
        { }

        public TreatmentViewEntityData(IEntityBase expert)
        {
            if (expert == null)
                return;
            Id = expert.Id;
            Update(expert);
        }

        public void Update(IEntityBase expert)
        {
            this.CopyFieldsFrom(expert);
        }
    }
}
