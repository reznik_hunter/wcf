﻿using Data;
using Data.Domain;
using Client.WPF.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.ViewModel.EntityLayer.EntityData
{
    class VisitViewEntityData : Notifier, IVisit, IViewEntityData
    {
        private int _id;
        private Expert _expert;
        private Patient _patient;
        private Treatment _treatment;
        private DateTime _date;
        private int _cost;

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
            }
        }

        public Expert Expert
        {
            get { return _expert; }
            set
            {
                _expert = value;
                NotifyPropertyChanged("Expert");
            }
        }

        public Patient Patient
        {
            get { return _patient; }
            set
            {
                _patient = value;
                NotifyPropertyChanged("Patient");
            }
        }
        public Treatment Treatment
        {
            get { return _treatment; }
            set
            {
                _treatment = value;
                NotifyPropertyChanged("Treatment");
            }
        }
        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                NotifyPropertyChanged("Date");
            }
        }
        public int Cost
        {
            get { return _cost; }
            set
            {
                _cost = value;
                NotifyPropertyChanged("Cost");
            }
        }

        public VisitViewEntityData()
        { }

        public VisitViewEntityData(IEntityBase expert)
        {
            if (expert == null)
                return;
            Id = expert.Id;
            Update(expert);
        }

        public void Update(IEntityBase expert)
        {
            this.CopyFieldsFrom(expert);
        }
    }
}
