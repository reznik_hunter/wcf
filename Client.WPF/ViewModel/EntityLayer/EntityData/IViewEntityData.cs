﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.ViewModel.EntityLayer.EntityData
{
    public interface IViewEntityData : IEntityBase
    {
        void Update(IEntityBase entity);
    }
}
