﻿using Client.WPF.Utils;
using Data;
using Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.ViewModel.EntityLayer.EntityData
{
    class PatientViewEntityData : Notifier, IPatient, IViewEntityData
    {
        private int _id;
        private string _fio;
        private string _anamnesis;
        private string _address;
        private string _phone;

        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                NotifyPropertyChanged("Id");
            }
        }

        public string Fio
        {
            get { return _fio; }
            set
            {
                _fio = value;
                NotifyPropertyChanged("Fio");
            }
        }

        public string Anamnesis
        {
            get { return _anamnesis; }
            set
            {
                _anamnesis = value;
                NotifyPropertyChanged("Anamnesis");
            }
        }

        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                NotifyPropertyChanged("Address");
            }
        }

        public string Phone
        {
            get { return _phone; }
            set
            {
                _phone = value;
                NotifyPropertyChanged("Phone");
            }
        }
        
        public PatientViewEntityData()
        { }

        public PatientViewEntityData(IEntityBase expert)
        {
            if (expert == null)
                return;
            Id = expert.Id;
            Update(expert);
        }

        public void Update(IEntityBase expert)
        {
            this.CopyFieldsFrom(expert);
        }
    }
}
