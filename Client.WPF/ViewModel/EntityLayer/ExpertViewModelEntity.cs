﻿//using Client.WPF.Commands;
//using Client.WPF.Model;
//using Client.WPF.Model.EntityLayer;
//using Client.WPF.Utils;
//using Client.WPF.ViewModel.TableLayer;
//using Client.WPF.ViewModel.ViewModelEntityData;
//using Data.Domain;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows.Input;

//namespace Client.WPF.ViewModel.EntityLayer
//{
//    class ExpertViewModelEntity : Notifier, IViewModelEntity<IViewModelData>
//    {
//        public const string PROPERTY_NAME = "ExpertViewModel";

//        private readonly IViewModelTable<Experts> _viewModelTable;
//        private readonly IModelEntity<Experts> _model;

//        private IViewModelData _viewModelData;
        
//        private readonly ICommand _updateCommand;
//        private readonly ICommand _createCommand;

//        private bool _detailsUpdated;
//        private bool _detailsCreated;

//        public Experts Entity 
//        { 
//            get 
//            { 
//                return _model.Entity; 
//            } 
//        }

//        public IViewModelData ViewModelData
//        {
//            get { return _viewModelData; }
//            set
//            {
//                _viewModelData = value;
//                NotifyPropertyChanged(PROPERTY_NAME);

//                if (value.Id == default(int))
//                {
//                    DetailsUpdated = false;
//                    DetailsCreated = true;
//                }
//                else
//                {            
//                    DetailsUpdated = true;
//                    DetailsCreated = false;                    
//                }
//            }
//        }

//        public ICommand UpdateCommand
//        {
//            get { return _updateCommand; }
//        }
        
//        public ICommand CreateCommand
//        {
//            get { return _createCommand; }
//        }

//        public bool DetailsUpdated
//        {
//            get { return _detailsUpdated; }
//            set
//            {
//                _detailsUpdated = value;
//                NotifyPropertyChanged("DetailsUpdated");
//            }
//        }

//        public bool DetailsCreated
//        {
//            get { return _detailsCreated; }
//            set
//            {
//                _detailsCreated = value;
//                NotifyPropertyChanged("DetailsCreated");
//            }
//        }

//        public ExpertViewModelEntity(IViewModelTable<Experts> viewModelTable)
//        {
//            _viewModelTable = viewModelTable;
//            _model = new BaseModelEntity<Experts>();
//            _model.Created +=
//                model_Сreated;
//            _createCommand = new CreateCommand<IViewModelData>(this);
//            ViewModelData = new ExpertViewModelData();
//        }

//        public ExpertViewModelEntity(IViewModelTable<Experts> viewModelTable, IModelEntity<Experts> model)
//        {
//            _viewModelTable = viewModelTable;
//            _model = model;
//            _model.Updated +=
//                model_Updated;
//            _updateCommand = new UpdateCommand<IViewModelData>(this);
//            ViewModelData = new ExpertViewModelData(_model.Entity);
//        }

//        public void Update()
//        {
//            var entity = new Experts();
//            entity.CopyFieldsFrom(ViewModelData);
//            _model.Update(ViewModelData.Id, entity);
//            _viewModelTable.Model.Update();
//        }

//        private void model_Updated(object sender,
//                                          CustomEventArgs<Experts> e)
//        {
//            Entity.CopyFieldsFromEntity(e.Entity);

//            if (ViewModelData != null
//                && e.Entity.Id == ViewModelData.Id)
//            {
//                ViewModelData.Update(e.Entity);
//            }
//        }

//        public void Create()
//        {
//            var entity = new Experts();
//            entity.CopyFieldsFrom(ViewModelData);
//            _model.Post(entity);
//            _viewModelTable.Model.Update();
//        }

//        private void model_Сreated(object sender,
//                                          CustomEventArgs<Experts> e)
//        {
//            Entity.CopyFieldsFromEntity(e.Entity);
//            DetailsCreated = false;
//        }
//    }
//}
