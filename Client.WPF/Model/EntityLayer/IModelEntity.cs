﻿using Client.WPF.Utils;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.Model.EntityLayer
{
    public interface IModelEntity<T> where T : IEntityBase
    {
        T Entity { get; set; }

        event EventHandler<CustomEventArgs<T>> Updated; 
        event EventHandler<CustomEventArgs<T>> Created; 

        T FindById(int id);
        void Post(T entity);
        void Update(int id, T entity);
        void Delete(int id);
    }
}
