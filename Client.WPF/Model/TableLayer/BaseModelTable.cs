﻿using Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.Model.TableLayer
{
    class BaseModelTable<T> : IModelTable<T> where T : IEntityBase
    {
        public ObservableCollection<T> CollectionEntity { get; set; }
        private string _path;

        public BaseModelTable()
        {
            _path = typeof(T).Name + "s";
            CollectionEntity = new ObservableCollection<T>();
            Load();
        }

        public void Load()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["RestServiceURL"]);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync("api/" + _path + "/").Result;
                if (response.IsSuccessStatusCode)
                {
                    var list = response.Content.ReadAsAsync<IEnumerable<T>>().Result.ToList();
                    foreach (var obj in list)
                    {
                        CollectionEntity.Add(obj);
                    }
                }
            }
        }

        public void Update()
        {
            Clear();
            Load();
        }

        public void Clear()
        {
            CollectionEntity.Clear();
        }
    }
}
