﻿using Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.WPF.Model.TableLayer
{
    public interface IModelTable<T>
        where T : IEntityBase
    {
        ObservableCollection<T> CollectionEntity { get; set; }

        void Load();
        void Update();
        void Clear();
    }
}
