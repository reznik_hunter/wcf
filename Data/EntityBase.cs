﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Data
{
    public class EntityBase : IEntityBase
    {
        public virtual int Id { get; set; }
    }
}