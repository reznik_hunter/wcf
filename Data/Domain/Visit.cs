﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Data.Domain
{
    public class Visit : EntityBase, IVisit
    {
        public virtual Expert Expert { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual Treatment Treatment { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual int Cost { get; set; }
    }
}