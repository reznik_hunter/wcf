﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Domain
{
    public interface IPatient : IEntityBase
    {
        string Fio { get; set; }
        string Anamnesis { get; set; }
        string Address { get; set; }
        string Phone { get; set; }
    }
}
