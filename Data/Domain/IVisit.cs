﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Domain
{
    public interface IVisit : IEntityBase
    {
        Expert Expert { get; set; }
        Patient Patient { get; set; }
        Treatment Treatment { get; set; }
        DateTime Date { get; set; }
        int Cost { get; set; }
    }
}
