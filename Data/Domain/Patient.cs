﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Data.Domain
{
    public class Patient : EntityBase, IPatient
    {
        public virtual string Fio { get; set; }
        public virtual string Anamnesis { get; set; }
        public virtual string Address { get; set; }
        public virtual string Phone { get; set; }
        //public virtual IList<Visit> Visits { get; set; }
    }
}