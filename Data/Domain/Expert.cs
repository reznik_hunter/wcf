﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Data.Domain
{
    public class Expert : EntityBase, IExpert
    {
        public virtual string Fio { get; set; }
        public virtual string Specialty { get; set; }
        public virtual string Address { get; set; }
        public virtual string Phone { get; set; }
        //public virtual IList<Visit> Visits { get; set; }
    }
}