﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Domain
{
    public interface ITreatment : IEntityBase
    {
        string Diagnosis { get; set; }
        string Medicine { get; set; }
        string Material { get; set; }
    }
}
