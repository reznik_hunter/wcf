﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Domain
{
    public interface IExpert : IEntityBase
    {
        string Fio { get; set; }
        string Specialty { get; set; }
        string Address { get; set; }
        string Phone { get; set; }
    }
}
