﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Data.Domain
{
    public class Treatment : EntityBase, ITreatment
    {
        public virtual string Diagnosis { get; set; }
        public virtual string Medicine { get; set; }
        public virtual string Material { get; set; }
        //public virtual IList<Visit> Visits { get; set; }
    }   
}