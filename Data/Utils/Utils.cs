﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Data.Utils
{
    public static class Utils
    {
        public static void CopyFieldsFrom<T>(this T obj, T entity) where T : IEntityBase
        {
            Type objType = obj.GetType();
            if (objType.ToString() != entity.GetType().ToString())
                return;
            PropertyInfo[] properties = objType.GetProperties();
            foreach(PropertyInfo objProperty in properties)
            {
                PropertyInfo entityProperty = entity.GetType().GetProperty(objProperty.Name);
                objProperty.SetValue(obj, entityProperty.GetValue(entity, null));
            }
        }
    }
}
