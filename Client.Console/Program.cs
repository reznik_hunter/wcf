﻿using Data.Domain;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Client.Console
{
    class Program
    {
        static void Main()
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:61982/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                
                HttpResponseMessage response = await client.GetAsync("api/Experts/4");
                if (response.IsSuccessStatusCode)
                {
                    Expert ex = await response.Content.ReadAsAsync<Expert>();
                    System.Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", ex.Id, ex.Fio, ex.Specialty, ex.Address, ex.Phone);
                    System.Console.ReadKey();
                }

                var exp = new Expert() { Fio = "A" , Specialty = "gg", Address = "ggg", Phone = "gggg"};
                exp.Id = 4;
                response = await client.PutAsJsonAsync("api/Experts/4", exp);
                response = await client.DeleteAsync("api/Experts/10");
                //response = await client.PostAsJsonAsync("api/Experts", exp);
                //if (response.IsSuccessStatusCode)
                //{
                //    Uri gizmoUrl = response.Headers.Location;

                //    // HTTP PUT
                //    exp.Id = 10;
                //    exp.Phone = "ffff";   // Update price
                //    response = await client.PutAsJsonAsync("api/Experts/10", exp);

                //    // HTTP DELETE
                //    response = await client.DeleteAsync(gizmoUrl);
                //}
            }
        }
    }
}
