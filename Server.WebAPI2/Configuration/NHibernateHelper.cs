﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Server.WebAPI2.Mapping;
using NHibernate;
using NHibernate.Context;
using NHibernate.Tool.hbm2ddl;

namespace Server.WebAPI2.Configuration
{
    public static class NHibernateHelper
    {
        public static ISessionFactory InitializeSessionFactory()
        {
            return Fluently.Configure().Database(
                MySQLConfiguration.Standard.ConnectionString(
                    cs => cs.Server("localhost").
                        Database("dbnulldev").
                        Username("root").Password("qwerty12345"))
                        //.ShowSql()
                ).Mappings(m => m.FluentMappings.AddFromAssemblyOf<ExpertMap>())
                .CurrentSessionContext<WebSessionContext>()
                //.ExposeConfiguration(cfg => new SchemaExport(cfg).Create(true,true))
                .BuildSessionFactory();   
        }
    }
}