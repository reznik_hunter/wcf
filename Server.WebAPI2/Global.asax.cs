﻿using Newtonsoft.Json;
using NHibernate;
using NHibernate.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Server.WebAPI2
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static ISessionFactory _sessionFactory;

        public static ISessionFactory SessionFactory
        {
            get
            {
                return _sessionFactory;
            }
        }

        public override void Init()
        {
            this.BeginRequest += (sender, e) =>
            {
                var session = _sessionFactory.OpenSession();
                CurrentSessionContext.Bind(session);
            };

            this.EndRequest += (sender, e) =>
            {
                var session = CurrentSessionContext.Unbind(_sessionFactory);
                session.Dispose();
            };

            base.Init();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            //{
            //    Formatting = Newtonsoft.Json.Formatting.Indented,
            //    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            //};

            _sessionFactory = Configuration.NHibernateHelper.InitializeSessionFactory();
        }
    }
}
