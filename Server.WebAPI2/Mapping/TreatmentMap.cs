﻿using FluentNHibernate.Mapping;
using Data.Domain;

namespace Server.WebAPI2.Mapping
{
    public class TreatmentMap : ClassMap<Treatment>
    {
        public TreatmentMap()
        {
            Table("treatments");
            Id(x => x.Id).GeneratedBy.Identity().Not.Nullable(); 
            Map(x => x.Diagnosis);
            Map(x => x.Medicine);
            Map(x => x.Material);
            //HasMany(x => x.Visits).Cascade.None().Table("visits").Inverse();//.Not.LazyLoad()
        }
    }
}