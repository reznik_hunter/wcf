﻿using FluentNHibernate.Mapping;
using Data.Domain;

namespace Server.WebAPI2.Mapping
{
    public class ExpertMap : ClassMap<Expert>
    {
        public ExpertMap()
        {
            Table("experts");
            Id(x => x.Id).GeneratedBy.Identity().Not.Nullable(); 
            Map(x => x.Fio);
            Map(x => x.Specialty);
            Map(x => x.Address);
            Map(x => x.Phone);
           // HasMany(x => x.Visits).Cascade.None().Table("visits").Inverse();//.Not.LazyLoad()
        }
    }
}