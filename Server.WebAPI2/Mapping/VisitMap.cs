﻿using FluentNHibernate.Mapping;
using Data.Domain;

namespace Server.WebAPI2.Mapping
{
    public class VisitMap : ClassMap<Visit>
    {
        public VisitMap()
        {
            Table("visits");
            Id(x => x.Id).GeneratedBy.Identity().Not.Nullable();
            References(x => x.Expert).Not.LazyLoad();
            References(x => x.Patient).Not.LazyLoad();
            References(x => x.Treatment).Not.LazyLoad();
            Map(x => x.Date);//.Not.Nullable();
            Map(x => x.Cost);
        }
    }
}