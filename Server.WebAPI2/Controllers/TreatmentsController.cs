﻿using Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.WebAPI2.Controllers
{
    public class TreatmentsController : ApiController
    {
        private readonly DataAccessLayer.TreatmentsDaoImpl _treatmentsDao;

        public TreatmentsController()
        {
            _treatmentsDao = new DataAccessLayer.TreatmentsDaoImpl();
        }

        // GET: api/Treatment
        public IEnumerable<Treatment> Get()
        {
            return _treatmentsDao.GetAll();
        }

        // GET: api/Treatment/5
        public Treatment Get(int id)
        {
            return _treatmentsDao.GetObjectByID(id);
        }

        // POST: api/Treatment
        public void Post([FromBody]Treatment value)
        {
            _treatmentsDao.Create(value);
        }

        // PUT: api/Treatment/5
        public void Put(int id, [FromBody]Treatment value)
        {
            _treatmentsDao.Update(id, value);
        }

        // DELETE: api/Treatment/5
        public void Delete(int id)
        {
            _treatmentsDao.Delete(id);
        }
    }
}
