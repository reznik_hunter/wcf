﻿using Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.WebAPI2.Controllers
{
    public class ExpertsController : ApiController
    {
        private readonly DataAccessLayer.ExpertsDaoImpl _expertsDao;

        public ExpertsController()
        {
            _expertsDao = new DataAccessLayer.ExpertsDaoImpl();
        }

        // GET: api/Experts
        public IEnumerable<Expert> Get()
        {
            return _expertsDao.GetAll();
        }

        // GET: api/Experts/5
        public Expert Get(int id)
        {
            return _expertsDao.GetObjectByID(id);
        }

        // POST: api/Experts
        public void Post([FromBody]Expert value)
        {
            _expertsDao.Create(value);
        }

        // PUT: api/Experts/5
        public void Put(int id, [FromBody]Expert value)
        {
            _expertsDao.Update(id, value);
        }

        // DELETE: api/Experts/5
        public void Delete(int id)
        {
            _expertsDao.Delete(id);
        }
    }
}
