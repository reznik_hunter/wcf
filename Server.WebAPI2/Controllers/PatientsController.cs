﻿using Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.WebAPI2.Controllers
{
    public class PatientsController : ApiController
    {
        private readonly DataAccessLayer.PatientsDaoImpl _patientsDao;

        public PatientsController()
        {
            _patientsDao = new DataAccessLayer.PatientsDaoImpl();
        }

        // GET: api/Patients
        public IEnumerable<Patient> Get()
        {
            return _patientsDao.GetAll();
        }

        // GET: api/Patients/5
        public Patient Get(int id)
        {
            return _patientsDao.GetObjectByID(id);
        }

        // POST: api/Patients
        public void Post([FromBody]Patient value)
        {
            _patientsDao.Create(value);
        }

        // PUT: api/Patients/5
        public void Put(int id, [FromBody]Patient value)
        {
            _patientsDao.Update(id, value);
        }

        // DELETE: api/Patients/5
        public void Delete(int id)
        {
            _patientsDao.Delete(id);
        }
    }
}
