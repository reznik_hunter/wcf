﻿using Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.WebAPI2.Controllers
{
    public class VisitsController : ApiController
    {
        private readonly DataAccessLayer.VisitsDaoImpl _visitsDao;

        public VisitsController()
        {
            _visitsDao = new DataAccessLayer.VisitsDaoImpl();
        }

        // GET: api/Visits
        public IEnumerable<Visit> Get()
        {
            return _visitsDao.GetAll();
        }

        // GET: api/Visits/5
        public Visit Get(int id)
        {
            return _visitsDao.GetObjectByID(id);
        }

        // POST: api/Visits
        public void Post([FromBody]Visit value)
        {
            _visitsDao.Create(value);
        }

        // PUT: api/Visits/5
        public void Put(int id, [FromBody]Visit value)
        {
            _visitsDao.Update(id, value);
        }

        // DELETE: api/Visits/5
        public void Delete(int id)
        {
            _visitsDao.Delete(id);
        }
    }
}
