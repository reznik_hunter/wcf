﻿using Data;
using System;
using System.Collections.Generic;

namespace Server.WebAPI2.DataAccessLayer
{
    public interface IDaoRepository<T,V> where T : EntityBase
                                   where V : struct
    {
        IEnumerable<T> GetAll();
        T GetObjectByID(V v);
        void Create(T t);
        void Update(V v,T t);
        void Delete(V v);
    }
}