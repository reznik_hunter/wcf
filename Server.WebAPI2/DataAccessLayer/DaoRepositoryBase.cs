﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using Data;
using NHibernate.Criterion;
using Data.Utils;

namespace Server.WebAPI2.DataAccessLayer
{
    public abstract class DaoRepositoryBase<T, V> : IDaoRepository<T,V> where T : EntityBase
                                   where V : struct
    {
        private readonly ISession _currentSession;

        protected DaoRepositoryBase()
        {
            _currentSession = WebApiApplication.SessionFactory.GetCurrentSession();
        }

        public IEnumerable<T> GetAll()
        {
            var criteria = this._currentSession.CreateCriteria(typeof(T));
            return criteria.List<T>();
        }

        public T GetObjectByID(V v)
        {
            return this._currentSession.CreateCriteria(typeof(T)).Add(Restrictions.Eq("Id", v)).UniqueResult<T>();          
        }

        public void Create(T t)
        {
            using (var transaction = this._currentSession.BeginTransaction())
            {
                try
                {
                    this._currentSession.Save(t);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
             }
        }

        public void Update(V v, T t)
        {
            if (t == null)
                return;
            var obj = this._currentSession.CreateCriteria(typeof(T)).Add(Restrictions.Eq("Id", v)).UniqueResult<T>(); 
            using (var transaction = this._currentSession.BeginTransaction())
            {
                try
                {
                    int id = obj.Id;
                    obj.CopyFieldsFrom(t);
                    obj.Id = id;
                    this._currentSession.Update(obj);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
                
                    
        }

        public void Delete(V v)
        {
            using (var transaction = this._currentSession.BeginTransaction())
            {
                
                var obj = (T)this._currentSession.Load(typeof(T), v);

                if(obj != null)
                {
                    try
                    {
                        this._currentSession.Delete(obj);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
                
            }
        }
    }
}